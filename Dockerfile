# Modeled after:
# https://serverfault.com/questions/1011561/docker-based-reverse-proxy-with-nginx-for-multiple-domains

FROM nginx:stable-alpine

#COPY etc-nginx-conf.d  /etc/nginx/conf.d

EXPOSE 80/tcp
EXPOSE 443/tcp

CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;';"]

WORKDIR /usr/share/nginx/html
